import React, {useState} from 'react';
import './Register.css';
import {Link} from "react-router-dom";
import {useDispatch, useSelector} from "react-redux";
import {registerUser} from "../../store/actions/usersActions";

const Register = () => {

    const [user, setUser] = useState({
        username: "",
        password: "",
    });

    const dispatch=useDispatch();
    const error = useSelector(state => state.users.registerError);

    const inputChangeHandler = e =>{
        const {name, value} = e.target;
        setUser(prevState => ({
            ...prevState,
            [name]: value
        }))
    };

    const submitFormHandler = e=>{
        e.preventDefault();
        dispatch(registerUser({...user}))
    };

    const getFieldError = fieldName =>{
        try{
            return error.errors[fieldName].message;
        } catch (e){
            return undefined;
        }
    };

    let usernameError = getFieldError('username');
    let passwordError = getFieldError('password');

    return (
        <div>
            <div className='form-box'>
                <h1>Sign up</h1>
                <form className='form'>
                    <input
                        type="text"
                        placeholder="Username"
                        name="username"
                        autoComplete="off"
                        onChange={inputChangeHandler}
                        value={user.username}
                    />
                    {usernameError && (
                        <p className='error'>{usernameError}</p>
                    )}
                    <input
                        type="password"
                        placeholder="Password"
                        name="password"
                        autoComplete="off"
                        onChange={inputChangeHandler}
                        value={user.password}
                    />
                    {usernameError && (
                        <p className='error'>{passwordError}</p>
                    )}
                    <button type="submit" onClick={submitFormHandler}>Sign up</button>
                    <Link to="/login" >
                        Already have an account? <span>Log in</span>
                    </Link>
                </form>
            </div>

        </div>
    );
};

export default Register;