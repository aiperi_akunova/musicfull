import React, {useEffect} from 'react';
import {useDispatch, useSelector} from "react-redux";
import {fetchAlbums} from "../../store/actions/albumsAction";
import AlbumsForm from "../../components/AlbumsForm/AlbumsForm";
import './Albums.css';
import Spinner from "../../components/Spinner/Spinner";

const Albums = () => {

    const params = new URLSearchParams(document.location.search);
    const artist = params.get('artist');

    const dispatch = useDispatch();
    const albums = useSelector(state => state.albums.albums);

    const loading = useSelector(state => state.albums.fetchLoadingAlbums);

    let artistName;
    if (albums.length !== 0) {
        artistName = albums[0].artist.name;
    }


    useEffect(() => {
        dispatch(fetchAlbums(artist));
    }, [dispatch, artist])

    return (
        <div>

            <div className='artist-name'><h1>{artistName}</h1></div>
            {loading ? (<Spinner/>
            ) : (
                <div className='album'>
                    {albums && albums.map(a => (
                        <AlbumsForm
                            key={a._id}
                            image={a.image}
                            title={a.title}
                            year={a.year}
                            id={a._id}
                        />
                    ))}
                </div>)}
        </div>
    )
};

export default Albums;