import React, {useEffect} from 'react';
import {useDispatch, useSelector} from "react-redux";
import {fetchArtists} from "../../store/actions/artistsActions";
import ArtistForm from "../../components/ArtistForm/ArtistForm";
import './Artists.css';
import Spinner from "../../components/Spinner/Spinner";

const Artists = () => {
    const dispatch = useDispatch();
    const artists = useSelector(state => state.artists.artists);
    const fetchLoading = useSelector(state => state.artists.fetchLoading);
    console.log(artists);

    useEffect(()=>{
        dispatch(fetchArtists())
    },[dispatch]);


    return (
        <div>
            {fetchLoading ? (<Spinner/>
                ):
                ( <div  className='artists'>
                    {artists && artists.map(artist => (
                        <ArtistForm
                            title={artist.name}
                            image={artist.image}
                            key={artist._id}
                            id={artist._id}
                        />
                    ))}
                </div>
                )}
        </div>
    );
};

export default Artists ;