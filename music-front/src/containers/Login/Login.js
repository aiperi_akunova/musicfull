import React, {useState} from 'react';
import './Login.css';
import {useDispatch, useSelector} from "react-redux";
import {loginUser} from "../../store/actions/usersActions";
import {Link} from "react-router-dom";

const Register = () => {

    const [user, setUser] = useState({
        username: "",
        password: "",
    });

    const dispatch = useDispatch();
    const error = useSelector(state => state.users.loginError);
    const loading = useSelector((state => state.users.loginLoading))



    const inputChangeHandler = e =>{
        const {name, value} = e.target;
        setUser(prevState => ({
            ...prevState,
            [name]: value
        }))
    };

    const submitFormHandler = e=>{
        e.preventDefault();
        dispatch(loginUser({...user}));
    };



    return (
        <div>
            <div className='form-box'>
                <h1>Log in</h1>
                <form className='form'>
                    {error && (
                        <p className='error'>{error.message}</p>
                    )}
                    <input
                        type="text"
                        placeholder="Username"
                        name="username"
                        autoComplete="off"
                        onChange={inputChangeHandler}
                        value={user.username}
                    />

                    <input
                        type="password"
                        placeholder="Password"
                        name="password"
                        autoComplete="off"
                        onChange={inputChangeHandler}
                        value={user.password}
                    />


                    <button
                        className='login-btn'
                        type="submit"
                        disabled={loading}
                        onClick={submitFormHandler}>
                        Log in
                    </button>

                    <Link to="/register" >
                        Don't have an account? <span>Sign in</span>
                    </Link>
                </form>
            </div>

        </div>
    );
};

export default Register;