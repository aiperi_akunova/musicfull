import React, {useEffect} from 'react';
import './Tracks.css';
import {useDispatch, useSelector} from "react-redux";
import {Redirect} from 'react-router-dom';
import {fetchTracks} from "../../store/actions/tracksActions";
import TracksForm from "../../components/TracksForm/TracksForm";
import Spinner from "../../components/Spinner/Spinner";

const Tracks = () => {

    const params = new URLSearchParams(document.location.search);
    const album = params.get('album');

    const dispatch = useDispatch();
    const tracks = useSelector(state => state.tracks.tracks);
    const loading = useSelector(state => state.tracks.fetchLoadingTracks);
    const user =useSelector(state => state.users.user);


    useEffect(() => {
        dispatch(fetchTracks(album));
    }, [dispatch, album]);

    if(!user){
        return <Redirect to="/login"/>
    }

    let trackTitle;
    let artistName;
    if(tracks.length !== 0){
        trackTitle=tracks[0].album.title;
        artistName=tracks[0].album.artist.name
    }


    return (

        <>{loading ? (<Spinner/>
        ) : (
            <div>
                <div className='track-title'>
                    <h1>{artistName}</h1>
                    <h3>{trackTitle}</h3>
                </div>
                <div className='tracks'>
                    {tracks && tracks.map(t => (
                        <TracksForm
                            key={t._id}
                            title={t.title}
                            number={t.numberOfTrack}
                            duration={t.duration}
                        />
                    ))}
                </div>
            </div>)}
        </>

    );
};

export default Tracks;