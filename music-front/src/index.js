import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import App from './App';
import {Router} from "react-router-dom";
import {applyMiddleware, combineReducers, compose, createStore} from "redux";
import {Provider} from "react-redux";
import {ToastContainer} from "react-toastify";
import thunk from "redux-thunk"
import history from "./history";
import artistsReducer from "./store/reducers/artistsReducer"
import albumsReducer from "./store/reducers/albumsReducer";
import tracksReducer from "./store/reducers/tracksReducer";
import 'react-toastify/dist/ReactToastify.css';
import usersReducer from "./store/reducers/usersReducer";


const composeEnhancers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;


const rootReducer = combineReducers({
   artists: artistsReducer,
    albums: albumsReducer,
    tracks: tracksReducer,
    users: usersReducer,
})

const store = createStore(rootReducer, composeEnhancers(
    applyMiddleware(thunk)
));


const app = (
    <Provider store={store}>
        <Router history={history}>
            <ToastContainer/>
            <App/>
        </Router>
    </Provider>
)

ReactDOM.render(app, document.getElementById('root'));
