import {FETCH_TRACKS_FAILURE, FETCH_TRACKS_REQUEST, FETCH_TRACKS_SUCCESS} from "../actions/tracksActions";

const initialState={
    fetchLoadingTracks: false,
    tracks: [],
    error: null,
}
const tracksReducer = (state = initialState, action) => {
    switch (action.type) {
        case FETCH_TRACKS_REQUEST:
            return {...state, fetchLoadingTracks: true, error: null};
        case FETCH_TRACKS_SUCCESS:
            return {...state, fetchLoadingTracks: false, tracks: action.payload};
        case FETCH_TRACKS_FAILURE:
            return {...state, fetchLoadingTracks: false, error: action.payload};
        default:
            return state;
    }

};

export default tracksReducer;