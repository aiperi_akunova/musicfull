import {FETCH_ALBUMS_FAILURE, FETCH_ALBUMS_REQUEST, FETCH_ALBUMS_SUCCESS} from "../actions/albumsAction";

const initialState={
    fetchLoadingAlbums: false,
    albums: [],
    error: null,
}
const albumsReducer = (state = initialState, action) => {
    switch (action.type) {
        case FETCH_ALBUMS_REQUEST:
            return {...state, fetchLoadingAlbums: true, error: null};
        case FETCH_ALBUMS_SUCCESS:
            return {...state, fetchLoadingAlbums: false, albums: action.payload};
        case FETCH_ALBUMS_FAILURE:
            return {...state, fetchLoadingAlbums: false, error: action.payload};
        default:
            return state;
    }

};

export default albumsReducer;