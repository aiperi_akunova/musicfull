import {
    FETCH_ARTISTS_FAILURE,
    FETCH_ARTISTS_REQUEST,
    FETCH_ARTISTS_SUCCESS,
} from "../actions/artistsActions";

const initialState={
    fetchLoading: false,
    artists: [],
    error: null,
}
const artistsReducer = (state = initialState, action) => {
switch (action.type) {
    case FETCH_ARTISTS_REQUEST:
        return {...state, fetchLoading: true, error: null};
    case FETCH_ARTISTS_SUCCESS:
        return {...state, fetchLoading: false, artists: action.payload};
    case FETCH_ARTISTS_FAILURE:
        return {...state, fetchLoading: false, error: action.payload};
    default:
        return state;
}

};

export default artistsReducer;