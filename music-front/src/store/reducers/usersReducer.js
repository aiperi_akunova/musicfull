import {
    LOGIN_USER_FAILURE,
    LOGIN_USER_REQUEST, LOGIN_USER_SUCCESS,
    REGISTER_USER_FAILURE,
    REGISTER_USER_REQUEST,
    REGISTER_USER_SUCCESS
} from "../actions/usersActions";

const initialState ={
    user: null,
    registerError: null,
    registerLoading: false,
    loginError:null,
    loginLoading:false,
};

const usersReducer = (state=initialState, action)=>{
    switch (action.type){
        case REGISTER_USER_REQUEST:
            return {...state, registerLoading: true, registerError: null}
        case REGISTER_USER_SUCCESS:
            return {...state, user: action.payload, registerError: null, registerLoading: false};
        case REGISTER_USER_FAILURE:
            return {...state, registerError: action.payload, registerLoading: false};
        case LOGIN_USER_REQUEST:
            return {...state, loginLoading: true, loginError: null}
        case LOGIN_USER_SUCCESS:
            return {...state, loginLoading: false, loginError: null, user: action.payload};
        case LOGIN_USER_FAILURE:
            return {...state, loginLoading: false, loginError: action.payload};
        default:
            return state;
    }
}

export default usersReducer;