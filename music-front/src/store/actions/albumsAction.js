import axios from "axios";
import {apiURL} from "../../config";
import {toast} from "react-toastify";

export const FETCH_ALBUMS_REQUEST = 'FETCH_ALBUMS_REQUEST';
export const FETCH_ALBUMS_SUCCESS = 'FETCH_ALBUMS_SUCCESS';
export const FETCH_ALBUMS_FAILURE = 'FETCH_ALBUMS_FAILURE';

export const fetchAlbumsRequest = ()=>({type: FETCH_ALBUMS_REQUEST});
export const fetchAlbumsSuccess = (data)=>({type: FETCH_ALBUMS_SUCCESS, payload: data});
export const fetchAlbumsFailure = (error)=>({type: FETCH_ALBUMS_FAILURE, payload: error});

export const fetchAlbums = (query)=>{
    return async dispatch =>{
        try {
            dispatch(fetchAlbumsRequest());
            const response = await axios.get(apiURL+'/albums?artist='+query);
            dispatch(fetchAlbumsSuccess(response.data));
        } catch (e) {
            dispatch(fetchAlbumsFailure(e));
            toast.error('Could not fetch tracks');
        }
    }
}