import axios from "axios";
import {apiURL} from "../../config";
import {toast} from "react-toastify";

export const FETCH_ARTISTS_REQUEST = 'FETCH_ARTISTS_REQUEST';
export const FETCH_ARTISTS_SUCCESS = 'FETCH_ARTISTS_SUCCESS';
export const FETCH_ARTISTS_FAILURE = 'FETCH_ARTISTS_FAILURE';

export const fetchArtistsRequest = ()=>({type: FETCH_ARTISTS_REQUEST});
export const fetchArtistsSuccess = (data)=>({type: FETCH_ARTISTS_SUCCESS, payload: data});
export const fetchArtistsFailure = (error)=>({type: FETCH_ARTISTS_FAILURE, payload: error});

export const fetchArtists = ()=>{
    return async dispatch =>{
        try {
            dispatch(fetchArtistsRequest());
            const response = await axios.get(apiURL+'/artists');
            dispatch(fetchArtistsSuccess(response.data));

        } catch (e) {
            dispatch(fetchArtistsFailure(e));
            toast.error('Could not fetch artists');
        }
    }
}