import axios from "axios";
import {apiURL} from "../../config";
import {toast} from "react-toastify";

export const FETCH_TRACK_HISTORY_REQUEST = 'FETCH_TRACK_HISTORY_REQUEST';
export const FETCH_TRACK_HISTORY_SUCCESS = 'FETCH_TRACK_HISTORY_SUCCESS';
export const FETCH_TRACK_HISTORY_FAILURE = 'FETCH_TRACK_HISTORY_FAILURE';

export const CREATE_TRACK_HISTORY_REQUEST = 'CREATE_TRACK_HISTORY_REQUEST';
export const CREATE_TRACK_HISTORY_SUCCESS = 'CREATE_TRACK_HISTORY_SUCCESS';
export const CREATE_TRACK_HISTORY_FAILURE = 'CREATE_TRACK_HISTORY_FAILURE';

export const fetchTracksHistoryRequest = ()=>({type: FETCH_TRACK_HISTORY_REQUEST});
export const fetchTracksHistorySuccess = (data)=>({type: FETCH_TRACK_HISTORY_SUCCESS, payload: data});
export const fetchTracksHistoryFailure = (error)=>({type: FETCH_TRACK_HISTORY_FAILURE, payload: error});

export const createTracksHistoryRequest = ()=>({type: CREATE_TRACK_HISTORY_REQUEST});
export const createTracksHistorySuccess = ()=>({type: CREATE_TRACK_HISTORY_SUCCESS});
export const createTracksHistoryFailure = (error)=>({type: CREATE_TRACK_HISTORY_FAILURE});

export const fetchTracksHistory = ()=>{
    return async (dispatch, getState) =>{
        try {
            const headers={
                'Authorization': getState().users.user && getState().users.user.token
            }
            dispatch(fetchTracksHistoryRequest());
            const response = await axios.get(apiURL+'/track_history', {headers});
            dispatch(fetchTracksHistorySuccess(response.data));
        } catch (error) {
            dispatch(fetchTracksHistoryFailure(error));
            if(error.response.status === 401){
                toast.warning('You need to login')
            }else {
                toast.error('Could not fetch tracks');
            }
        }
    }
}

export const createTrackHistory = data => {
    return async (dispatch,getState) => {
        try {

            const headers={
                'Authorization': getState().users.user && getState().users.user.token
            }

            dispatch(createTracksHistoryRequest());

            await axios.post(apiURL+'/track_history', data, {headers});

            dispatch(createTracksHistorySuccess());
            toast.success('Product created!');
        } catch (e) {
            dispatch(createTracksHistoryFailure());
            throw e;
        }
    };
};
