import {historyPush} from "./historyActions";
import {apiURL} from "../../config";
import axios from "axios";
import {toast} from "react-toastify";

export const REGISTER_USER_REQUEST = 'REGISTER_USER_REQUEST';
export const REGISTER_USER_SUCCESS = 'REGISTER_USER_SUCCESS';
export const REGISTER_USER_FAILURE = 'REGISTER_USER_FAILURE';

export const LOGIN_USER_REQUEST = 'LOGIN_USER_REQUEST';
export const LOGIN_USER_SUCCESS = 'LOGIN_USER_SUCCESS';
export const LOGIN_USER_FAILURE = 'LOGIN_USER_FAILURE';


export const registerUserRequest = () => ({type: REGISTER_USER_REQUEST});
export const registerUserSuccess = user => ({type: REGISTER_USER_SUCCESS, payload: user});
export const registerUserFailure = error => ({type: REGISTER_USER_FAILURE, payload: error});

export const loginUserRequest = () => ({type: LOGIN_USER_REQUEST});
export const loginUserSuccess = user => ({type: LOGIN_USER_SUCCESS, payload: user});
export const loginUserFailure = error => ({type: LOGIN_USER_FAILURE, payload: error});


export const registerUser = userData => {
    return async dispatch => {
        try {
            const response = await axios.post(apiURL+'/users', userData);
            dispatch(registerUserSuccess(response.data));
            dispatch(historyPush('/'));
        } catch (error) {
            if (error.response && error.response.data) {
                dispatch(registerUserFailure(error.response.data));
            } else {
                dispatch(registerUserFailure({global: 'No internet'}));
                toast.error('No internet connection');
            }

        }
    }
};


export const loginUser = userData=>{
    return async dispatch =>{
        try {
            dispatch(loginUserRequest());
            const response = await axios.post(apiURL+'/users/sessions',userData);
            dispatch(loginUserSuccess(response.data.user));
            dispatch(historyPush('/'));
            toast.success('Login successful');
        }catch (error) {
            if(error.response && error.response.data){
                dispatch(loginUserFailure(error.response.data))
            }else {
                dispatch(loginUserFailure({global: 'No internet'}))
                toast.error('No internet connection');
            }
        }
    }
}
