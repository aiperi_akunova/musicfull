import axios from "axios";
import {apiURL} from "../../config";
import {toast} from "react-toastify";

export const FETCH_TRACKS_REQUEST = 'FETCH_TRACKS_REQUEST';
export const FETCH_TRACKS_SUCCESS = 'FETCH_TRACKS_SUCCESS';
export const FETCH_TRACKS_FAILURE = 'FETCH_TRACKS_FAILURE';

export const fetchTracksRequest = ()=>({type: FETCH_TRACKS_REQUEST});
export const fetchTracksSuccess = (data)=>({type: FETCH_TRACKS_SUCCESS, payload: data});
export const fetchTracksFailure = (error)=>({type: FETCH_TRACKS_FAILURE, payload: error});

export const fetchTracks = (query)=>{
    return async (dispatch, getState) =>{
        try {
            const headers={
                'Authorization': getState().users.user && getState().users.user.token
            }
            dispatch(fetchTracksRequest());
            const response = await axios.get(apiURL+'/tracks?album='+query, {headers});
            dispatch(fetchTracksSuccess(response.data));
        } catch (error) {
            dispatch(fetchTracksFailure(error));
            if(error.response.status === 401){
                toast.warning('You need to login')
            }else {
                toast.error('Could not fetch tracks');
            }
        }
    }
}