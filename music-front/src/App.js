import './App.css';
import {Route, Switch} from "react-router-dom";
import Layout from "./components/Layout/Layout";
import Artists from "./containers/Artists/Artists";
import Albums from "./containers/Albums/Albums";
import Tracks from "./containers/Tracks/Tracks";
import Register from "./containers/Register/Register";
import Login from "./containers/Login/Login";
import TrackHistory from "./containers/TrackHistory/TrackHistory";

const App = () =>
    (
        <Layout>
            <Switch>
                <Route path="/" exact component={Artists}/>
                <Route path="/albums" component={Albums}/>
                <Route path="/tracks" component={Tracks}/>
                <Route path="/register" component={Register}/>
                <Route path="/login" component={Login}/>
                <Route path="/track_history" component={TrackHistory}/>
                <Route render={() => <h1>Not found</h1>}/>
            </Switch>
        </Layout>
    );

export default App;
