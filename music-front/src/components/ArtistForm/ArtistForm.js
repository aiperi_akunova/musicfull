import React from 'react';
import './ArtistForm.css';
import noImage from '../../assets/images/no-image.png';
import {apiURL} from "../../config";
import {Link} from "react-router-dom";


const ArtistForm = ({title,image, id}) => {


    let cardImage = noImage;

    if (image) {
        cardImage = apiURL + '/uploads/' + image;
    }


    return (
        <div className='albums-box'>
            <Link to={'albums?artist='+id}>
                <img src={cardImage} alt="Albums"/>
                <h3>{title}</h3>
            </Link>
        </div>
    );
};

export default ArtistForm;