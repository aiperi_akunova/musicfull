import noImage from "../../assets/images/no-album.jpg";
import {apiURL} from "../../config";
import React from "react";
import './AlbumsForm.css';
import {Link} from "react-router-dom";

const AlbumsForm = ({image, title, year, id}) => {
    let cardImage = noImage;

    if (image) {
        cardImage = apiURL + '/uploads/' + image;
    }

    return (
        <div className='albums-box'>
            <Link to={'/tracks?album='+id}>
                <img src={cardImage} alt="Album"/>
                <h3>{title}</h3>
                <p>Year - {year}</p>
            </Link>
        </div>
    );
};

export default AlbumsForm;