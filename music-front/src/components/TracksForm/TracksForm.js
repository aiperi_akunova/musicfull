import React from 'react';
import './TracksForm.css';

const TracksForm = ({number, title, duration}) => {
    return (
        <div className='track-box'>
            <p><b>#: </b>{number}</p>
            <p><b>Title: </b>{title}</p>
            <p><b>Duration: </b>{duration}</p>
        </div>
    );
};

export default TracksForm;