import React from 'react';
import './Layout.css';
import {Link} from "react-router-dom";
import {useSelector} from "react-redux";

const Layout = ({children}) => {
    const user = useSelector(state => state.users.user);

    return (
        <div className='Layout-main'>
            <div className="layout-header">
                <Link to='/' ><h1>Best music app :)</h1></Link>
                {user ? (
                    <div>
                        <span>{user.username}</span>
                        <Link to="/track_history">My Track History</Link>
                    </div>
                ):(
                <div>
                    <Link to="/register">Sign up</Link>
                    <Link to="/login">Log in</Link>
                </div>
                    )}
            </div>
            <main className="Layout-Content">
                {children}
            </main>
        </div>
    );
};

export default Layout;