const express = require('express');
const Track = require('../models/Track');
const auth = require("../middleware/auth");
const router = express.Router();

router.get('/', auth, async (req, res) =>{
  try{
    const query ={};
    if(req.query.album){
      query.album = req.query.album;

    }

    const tracks = await Track.find(query).sort('numberOfTrack').populate( {path: 'album', populate:{path:'artist', select:'name'}});
    res.send(tracks);
  }catch (e){
    res.sendStatus(500);
  }
});

router.post('/', async (req,res)=>{

  if(!req.body.title || !req.body.album || !req.body.numberOfTrack){
    return res.status(400).send('Data not valid');
  }

  const trackData = {
    title: req.body.title,
    album: req.body.album,
    duration: req.body.duration || null,
    numberOfTrack: req.body.numberOfTrack,
  };


  const track = new Track(trackData);

  try{
    await track.save();
    res.send(track);
  }catch (e) {
    res.status(400).send(e);
  }

});



module.exports = router;