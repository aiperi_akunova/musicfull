const express = require('express');
const User = require("../models/User");

const router = express.Router();

router.get('/', async (req, res) =>{
  try{
    const users = await User.find();
    res.send(users);
  }catch (e){
    res.sendStatus(500);
  }
});

router.post('/', async (req, res) => {
  try {
    const userData = {
      username: req.body.username,
      password: req.body.password,
    }

    const user = new User(userData);

    user.generateToken();
    await user.save();
    res.send(user);
  } catch (error) {
    res.status(400).send(error);
  }

});


router.post('/sessions',async (req, res) => {
  const user = await User.findOne({username: req.body.username});

  if (!user) {
    return res.status(401).send({message: "Credentials are wrong"});
  }

  const isMatch = await user.checkPassword(req.body.password);

  if (!isMatch) {
    return res.status(401).send({message: 'Credentials are wrong'});
  }

  try {
    user.generateToken();
    await user.save({validateBeforeSave:false});
    res.send({message: "Username and password correct!", user});
  } catch (error) {
    res.sendStatus(500);
  }

})


module.exports = router;