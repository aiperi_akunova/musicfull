const express = require('express');
const TrackHistory = require("../models/TrackHistory");
const auth = require("../middleware/auth");
const router = express.Router();

router.get('/', auth, async (req,res)=>{
  try{
    const trackHistory = await TrackHistory.find({user: req.user._id}).populate({path: 'track', select: 'title', populate:{path:'album', populate:{path:'artist', select:'name'}}});
    res.send(trackHistory);
  }catch (e){
    res.sendStatus(500);
  }
})



router.post('/', auth,async (req, res) => {

  if(!req.body.track){
    return res.status(400).send({error: 'Data is not valid'});
  }

  const date = new Date();
  const trackHistoryData = {
    user: req.user._id,
    track: req.body.track,
    datetime: date.toISOString(),
  };

  const trackHistory = new TrackHistory(trackHistoryData);

  try {
    await trackHistory.save();
    res.send(trackHistory);
  } catch (e) {
    res.status(400).send(e);
  }

});

module.exports = router;